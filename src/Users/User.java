/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import ElementPhoto.Album;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author kerly
 */
public class User implements Serializable{
    //Variables para la clase USER
    private ArrayList<Album> albums = new ArrayList<>();
    private String nickname;
    private String password;
//Constructor
    public User(String nickname, String password) {
        this.nickname = nickname;
        this.password = password;
    }
//Getters& Setters
    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
