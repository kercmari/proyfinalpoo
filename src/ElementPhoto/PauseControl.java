/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kerly
 */
public class PauseControl {
    // Varaibles de clase 
    private boolean needToPause;
//  metodo para esperar mientras corre el slideshow
    public synchronized void pausePoint() {
        while (needToPause) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }
    }

    public synchronized void pause() {
        needToPause = true;
    }

    public synchronized void unpause() {
        needToPause = false;
        this.notifyAll();
    }
}