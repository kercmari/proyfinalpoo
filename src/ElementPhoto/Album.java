/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author kerly
 */
public class Album implements Serializable{
    // Creacion de lista para guardar el objeto Photo y creacion de variables de la clase album
    private ArrayList<Photo> photos = new ArrayList<>();
    private String title;
    private String descripcion;
// Constructor
    public Album(String title, String descripcion) {
        this.title = title;
        this.descripcion = descripcion;
    }
    //Getters & Setters
    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    
}
