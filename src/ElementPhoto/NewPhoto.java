/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;

import java.time.ZoneOffset;
import java.util.Date;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import PhotoOrganizer.PhotoOrganizer;

/**
 *
 * @author kerly
 */
public class NewPhoto {

    private GridPane root;
    private Scene scene;
    private Stage stage;

    public NewPhoto(Album album, String ruta) {
        //String ruta, String lugar, Date fecha, Album album, String descripcion
        ImageView view = new ImageView(ruta);
        GridPane.setHalignment(view, HPos.CENTER);
        view.setPreserveRatio(true);
        view.setFitHeight(200);
        Label lblLugar = new Label("Lugar:");
        Label lblFecha = new Label("Fecha:");
        Label lblDescripcion = new Label("Descripcion:");
        TextField tfLugar = new TextField();
        tfLugar.setMaxWidth(315);
        DatePicker dpFecha = new DatePicker();
        TextArea taDescripcion = new TextArea();
        taDescripcion.setMaxWidth(315);
        Button btnAceptar = new Button("Aceptar");
        Button btnCancelar = new Button("Cancelar");
        HBox botones = new HBox(5, btnCancelar, btnAceptar);
        botones.setAlignment(Pos.CENTER);
        GridPane.setHalignment(botones, HPos.CENTER);
        btnCancelar.setOnAction(e -> {
            stage.close();
        });
        btnAceptar.setOnAction(e -> {
            if ((!tfLugar.getText().equals("") & dpFecha.getValue() != null & !taDescripcion.getText().equals(""))) {
                
                Photo photo = new Photo(ruta, lblLugar.getText(), Date.from(dpFecha.getValue().atStartOfDay().toInstant(ZoneOffset.UTC)), album, taDescripcion.getText());
                if (album.getPhotos().isEmpty()) {
  //                  PhotoOrganizer.gallery.getChildren().clear();
                }
                album.getPhotos().add(photo);
 //               PhotoOrganizer.gallery.getChildren().add(PhotoOrganizer.createPhotoView(photo));
 //               PhotoOrganizer.saveUsers();
                stage.close();
            }
        });
        root = new GridPane();
        root.setPrefSize(400, 600);
        root.setHgap(5);
        root.setVgap(5);
        root.setAlignment(Pos.CENTER);

        root.add(view, 0, 0, 2, 1);
        root.add(lblLugar, 0, 1);
        root.add(tfLugar, 1, 1);
        root.add(lblFecha, 0, 2);
        root.add(dpFecha, 1, 2);
        root.add(lblDescripcion, 0, 3);
        root.add(taDescripcion, 1, 3);
        root.add(botones, 0, 5, 2, 1);

        scene = new Scene(root);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Nueva Imagen");
        stage.show();
    }

    public Stage getStage() {
        return stage;
    }

}
