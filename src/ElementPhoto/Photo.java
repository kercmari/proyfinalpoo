/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/**
 *
 * @author kerly
 */
public class Photo implements Serializable {
// Variables de clase
    private final int[][] imageData;
    private final int w;
    private final int h;
    private ArrayList<String> personas = new ArrayList<>();
    private String lugar;
    private Date fecha;
    private Album album;
    private String descripcion;
// Constructor para la creacion del objeto photo
    public Photo(String ruta, String lugar, Date fecha, Album album, String descripcion) {
        // Codigo obtenido de @https://aprenderaprogramar.com/foros/index.php?topic=6761.0
        // Leer cada pixel de imagen y guardarlo en una matriz de bits
        Image image = new Image(ruta);
        w = ((int) image.getWidth());
        h = ((int) image.getHeight());
        imageData = new int[w][h];
        PixelReader read = image.getPixelReader();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                imageData[i][j] = read.getArgb(i, j);
            }
        }

        this.lugar = lugar;
        this.fecha = fecha;
        this.album = album;
        this.descripcion = descripcion;
        
    }
// crea un objeto tipo ImageView con los bits obtenidos de la matriz
    public ImageView getImageView() {

        WritableImage img = new WritableImage(w, h);

        PixelWriter write = img.getPixelWriter();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                write.setArgb(i, j, imageData[i][j]);
            }
        }
        return new ImageView(img);
    }
// Getters & Setters
    public ArrayList<String> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<String> personas) {
        this.personas = personas;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
