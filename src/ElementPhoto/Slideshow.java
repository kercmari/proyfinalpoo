/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;


import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author kerly
 */
public class Slideshow   {
// Variables de clase
    private VBox root;
    private Scene scene;
    private PauseControl pauseControl = new PauseControl();
    private Stage stage;
    private boolean run = true;
    private int s = 4;
// Constructor le envia el objeto album y crea un verticla box junto otros contenedores añadir el boton de play y pause
    // Instanciar y llamar a thread para cear un hilo que recorra las fotos y en una lapso de tiempo siga pasando a la siguiente,
    public Slideshow(Album album) {
        
        root = new VBox();
        root.setPrefWidth(600);
        root.setStyle("-fx-background-color: #7a7a7a;");
        StackPane container = new StackPane();
        container.setPrefSize(600, 600);

        HBox menu = new HBox(5);
        menu.setAlignment(Pos.CENTER);
        Button btnPlay = new Button("Pause");
        menu.getChildren().addAll( btnPlay);
        root.getChildren().addAll(container, menu);
        
        scene = new Scene(root);
        stage = new Stage();
        stage.setTitle("SlideShow");
        stage.setScene(scene);
        stage.show();

        Thread thread = new Thread() {
            @Override
            public void run() {
                while (run) {
                    for (Photo photo : album.getPhotos()) {
                        pauseControl.pausePoint();

                        Platform.runLater(() -> {
                            container.getChildren().clear();
                            ImageView view = photo.getImageView();
                            view.setPreserveRatio(true);
                            view.setFitHeight(600);
                            container.getChildren().add(view);
                        });
                        try {
                            Thread.sleep(s * 1000);
                        } catch (InterruptedException ex) {
                        }

                    }
                }
            }
        };
        thread.start();

        btnPlay.setOnAction(e -> {
            if (btnPlay.getText().equals("Pause")) {
                btnPlay.setText("Play");
                pauseControl.pause();
            } else {
                btnPlay.setText("Pause");
                pauseControl.unpause();
            }
        });
        stage.setOnCloseRequest(e -> {
            run = false;
        });

    }

}
