/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementPhoto;


import Users.User;
import java.time.ZoneOffset;
import java.util.Date;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import PhotoOrganizer.PhotoOrganizer;


/**
 *
 * @author kerly
 */
public class NewAlbum {
    // Varaibles para  los contenedores
    private GridPane root;
    private Scene scene;
    private Stage stage;
    
    //Constructor de newAlbum le envias el objeto usuario   para la creacion de un GridPane añadiendo los botones y dandole su respectivo evento como 
    //cancelar  y aceptar
    public NewAlbum(User user){
        Label lblNombre = new Label("Nombre:");
        Label lblDescripcion = new Label("Descripcion:");
        TextField tfNombre = new TextField();
        tfNombre.setMaxWidth(260);
        TextArea taDescripcion = new TextArea();
        taDescripcion.setMaxWidth(260);
        Button btnAceptar = new Button("Aceptar");
        Button btnCancelar = new Button("Cancelar");
        HBox botones = new HBox(5, btnCancelar, btnAceptar);
        botones.setAlignment(Pos.CENTER);
        GridPane.setHalignment(botones, HPos.CENTER);
        btnCancelar.setOnAction(e -> {
            stage.close();
        });
        btnAceptar.setOnAction(e -> {
            if ((!tfNombre.getText().equals("") & !taDescripcion.getText().equals(""))) {
                Album album = new Album(tfNombre.getText(),taDescripcion.getText());
                if (user.getAlbums().isEmpty()) {
      //              PhotoOrganizer.gallery.getChildren().clear();
                }
                user.getAlbums().add(album);
     //           PhotoOrganizer.gallery.getChildren().add(PhotoOrganizer.createAlbumView(album));
     //           PhotoOrganizer.saveUsers();
                stage.close();
            }
        });
        // Intanciar el pane donde va a contener todos los lables y botones
        root = new GridPane();
        root.setPrefSize(400, 400);
        root.setHgap(5);
        root.setVgap(5);
        root.setAlignment(Pos.CENTER);

        root.add(lblNombre, 0, 1);
        root.add(tfNombre, 1, 1);
        root.add(lblDescripcion, 0, 2);
        root.add(taDescripcion, 1, 2);
        root.add(botones, 0, 4, 2, 1);
        // Instanciar la scena para el contendor de mi GridPane y intanciar el stage del a ventana
        scene = new Scene(root);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Nuevo Album");
        stage.show();
    }
// Obtener el stage
    public Stage getStage() {
        return stage;
    }
}
