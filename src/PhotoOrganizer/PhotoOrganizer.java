/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoOrganizer;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */


import ElementPhoto.Album;
import ElementPhoto.NewAlbum;
import ElementPhoto.NewPhoto;
import ElementPhoto.Photo;
import ElementPhoto.Slideshow;
import Users.User;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author wesley
 */
public class PhotoOrganizer extends Application {

    public static TilePane gallery;
    private static Label headerTitle;
    public static Pane root;
    private static Pane headerButtons;
    private static Button addButton;
    private static Button backButton;
    private static Button slideButton;
    private static ToggleButton deleteButton;
    public static Scene mainScene;
    private ScrollPane scrollPane;
    private int HEIGHT = 700;
    private int WIDTH = 1050;

    public static ArrayList<User> users;
    private static User currentUser;
    private static Object currentObject;

    @Override
    public void start(Stage primaryStage) {
        loadUsers();

        currentUser = users.get(0);

        backButton = createBackButton();

        deleteButton = new ToggleButton("Delete");
        deleteButton.setLayoutX(WIDTH - 180);
        deleteButton.setLayoutY(HEIGHT - 70);
        
        slideButton = new Button("SlideShow");
        slideButton.setLayoutY(HEIGHT - 70);
        slideButton.setLayoutX(20);
        slideButton.setOnAction(e->{
            if (((Album)currentObject).getPhotos().size()>0){
                new Slideshow((Album)currentObject);
            }
        });

        System.out.println(currentUser.getNickname());
        /////////////////////////////////
        root = new Pane();
        root.setStyle("-fx-background-color: #7a7a7a;");

        mainScene = new Scene(root);

        primaryStage.setTitle("Photo Organizer");
        primaryStage.setWidth(WIDTH);
        primaryStage.setHeight(HEIGHT);
        primaryStage.setScene(mainScene);
        //////////////////////////////

        headerTitle = createTitleLabel();
        root.getChildren().add(headerTitle);

        headerButtons = createHeaderButtons();
        root.getChildren().add(headerButtons);

        gallery = createTilePane();
        gallery.setStyle("-fx-background-color: #d0cccc;");

        scrollPane = createScrollPane();
        scrollPane.setContent(gallery);
        scrollPane.setLayoutX(50);
        scrollPane.setLayoutY(100);
        root.getChildren().add(scrollPane);

        addButton = createAddButton(primaryStage);

        root.getChildren().add(addButton);
        root.getChildren().add(deleteButton);

        showUserAlbums(users.get(0));

        primaryStage.show();
    }

    private Label createTitleLabel() {
        Label label = new Label("PHOTO ORGANIZER");
        label.setTextFill(Color.web("#872323"));
        label.setFont(Font.font("Cambria", 50));
        label.setStyle("-fx-font-weight: bold");
        return label;
    }

    private ScrollPane createScrollPane() {
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Horizontal
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Vertical scroll bar
        sp.setFitToWidth(true);
        return sp;
    }

    private Pane createHeaderButtons() {
        HBox container = new HBox(10);
        Button filter = new Button("Filter");
        Button login = new Button("Login");
        //container.getChildren().addAll(filter);
        //container.getChildren().addAll(login);
        container.setLayoutX(WIDTH - 200);
        container.setLayoutY(20);
        return container;
    }

    private TilePane createTilePane() {
        TilePane tilePane = new TilePane();
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setPadding(new Insets(15, 15, 15, 15));
        tilePane.setVgap(30);
        tilePane.setHgap(20);
        tilePane.setPrefHeight(500);
        tilePane.setPrefWidth(WIDTH - 100);

        return tilePane;
    }

    public static Pane createAlbumView(Album album) {
        VBox vbox = new VBox();

        StackPane container = new StackPane();
        container.setOnMouseClicked(e -> {

            if (deleteButton.isSelected()) {
                Alert confirmacion = new Alert(Alert.AlertType.CONFIRMATION);
                confirmacion.setTitle("Eliminar");
                confirmacion.setHeaderText(null);
                confirmacion.initStyle(StageStyle.UTILITY);
                confirmacion.setContentText("¿Desea eliminar este album?");
                if (confirmacion.showAndWait().get() == ButtonType.OK) {
                    currentUser.getAlbums().remove(album);
                    showUserAlbums(currentUser);
                    saveUsers();
                }
            } else {

                headerButtons.getChildren().add(backButton);
                root.getChildren().add(slideButton);
                currentObject = album;
                showAlbumPhotos(album);
                e.consume();
            }
        });

        container.setPrefSize(250, 250);
        container.setStyle("-fx-border-color:black;-fx-border-insets:0;-fx-border-with:3;-fx-background-color:black");
        final ImageView imageView;
        if (album.getPhotos().size() != 0) {
            imageView = album.getPhotos().get(album.getPhotos().size() - 1).getImageView();
        } else {
            imageView = new ImageView("file:default-no-image.png");
        }

        imageView.setPreserveRatio(true);
        imageView.setFitWidth(250);

        container.getChildren().add(imageView);
        vbox.getChildren().add(container);

        Label titleLabel = new Label(album.getTitle());
        titleLabel.setTextFill(Color.web("#0000FF"));
        titleLabel.setStyle("-fx-font-weight: bold");
        titleLabel.setMaxWidth(250);
        vbox.getChildren().add(titleLabel);

        return vbox;
    }

    public static Pane createPhotoView(Photo photo) {

        VBox vbox = new VBox();
        StackPane container = new StackPane();
        container.setOnMouseClicked(e -> {
            if (deleteButton.isSelected()) {
                Alert confirmacion = new Alert(Alert.AlertType.CONFIRMATION);
                confirmacion.setTitle("Eliminar");
                confirmacion.setHeaderText(null);
                confirmacion.initStyle(StageStyle.UTILITY);
                confirmacion.setContentText("¿Desea eliminar esta imagen?");
                if (confirmacion.showAndWait().get() == ButtonType.OK) {
                    ((Album) currentObject).getPhotos().remove(photo);
                    showAlbumPhotos((Album) currentObject);
                    saveUsers();
                }
            } else {
                currentObject = photo;
                
                root.getChildren().remove(slideButton);
                root.getChildren().remove(addButton);
                root.getChildren().remove(deleteButton);
                showOpenPhoto(photo);
            }

            e.consume();
        });

        container.setPrefSize(187.5, 187.5);
        container.setStyle("-fx-border-color:black;-fx-border-insets:0;-fx-border-with:3;-fx-background-color:black");

        ImageView imageView = photo.getImageView();
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(187.5);

        container.getChildren().add(imageView);
        vbox.getChildren().add(container);

        Label titleLabel = new Label(photo.getDescripcion());
        titleLabel.setMaxWidth(187.5);
        vbox.getChildren().add(titleLabel);

        String fecha[] = photo.getFecha().toString().split(" ");
        Label yearLabel = new Label(fecha[2] + "/" + fecha[1] + "/" + fecha[5]);
        yearLabel.setTextFill(Color.web("#0000FF"));
        yearLabel.setStyle("-fx-font-weight: bold");
        vbox.getChildren().add(yearLabel);

        return vbox;
    }

    private static Pane createOpenPhotoView(Photo photo) {

        VBox vbox = new VBox();
        StackPane container = new StackPane();

        container.setPrefSize(500, 500);
        container.setStyle("-fx-border-color:black;-fx-border-insets:0;-fx-border-with:0;-fx-background-color:black");

        ImageView imageView = photo.getImageView();
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(500);

        container.getChildren().add(imageView);
        vbox.getChildren().add(container);

        Label titleLabel = new Label(photo.getDescripcion());

        String fecha[] = photo.getFecha().toString().split(" ");
        Label yearLabel = new Label(fecha[2] + "/" + fecha[1] + "/" + fecha[5]);

        return vbox;
    }

    private Button createAddButton(Stage stage) {
        Button button = new Button("ADD");
        button.setLayoutX(WIDTH - 100);
        button.setLayoutY(HEIGHT - 70);

        button.setOnAction(e -> {
            if (currentObject instanceof Album) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Seleccione sus imagenes");
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Imágenes", "*.jpg", "*.png", "*.gif", "*.bmp", "*.jpeg", "*.svg",
                                "*.JPG", "*.PNG", "*.GIF", "*.BMP", "*.JPEG", "*.SVG")
                );
                List<File> imgFiles = fileChooser.showOpenMultipleDialog(stage);
                if (imgFiles != null) {

                    for (File file : imgFiles) {
                        System.out.println(file.getAbsolutePath());
                        Stage stageNewPhoto = new NewPhoto((Album) currentObject, "file:" + file.getAbsolutePath()).getStage();

                    }

                }
            } else {
                new NewAlbum(currentUser);
            }

            e.consume();
        });
        return button;
    }

    private Button createBackButton() {
        Button button = new Button("Volver");
        button.setOnAction(e -> {
            gallery.getChildren().clear();

            if (currentObject instanceof Album) {
                showUserAlbums(currentUser);
                
                root.getChildren().remove(slideButton);
                currentObject = null;
            } else if (currentObject instanceof Photo) {
                showAlbumPhotos(((Photo) currentObject).getAlbum());
                currentObject = ((Photo) currentObject).getAlbum();
                
                root.getChildren().add(slideButton);
                root.getChildren().add(addButton);
                root.getChildren().add(deleteButton);
            }

            e.consume();

        });

        return button;
    }

    public static void showUserAlbums(User user) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    gallery.getChildren().clear();
                    headerTitle.setText("PHOTO ORGANIZER");
                    if (headerButtons.getChildren().contains(backButton)) {
                        headerButtons.getChildren().remove(backButton);
                    }
                    if (user.getAlbums().size() > 0) {
                        for (Album album : user.getAlbums()) {
                            gallery.getChildren().add(createAlbumView(album));
                        }
                    } else {
                        gallery.getChildren().add(new Label("No existe ningun album aún . . ."));
                    }
                });
            }
        };
        thread.start();
    }

    private static void showAlbumPhotos(Album album) {
        Thread thread = new Thread() {
            @Override
            public void run() {

                Platform.runLater(() -> {
                    gallery.getChildren().clear();
                    headerTitle.setText(album.getTitle());

                    if (album.getPhotos().size() > 0) {
                        for (int i = album.getPhotos().size() - 1; i >= 0; i--) {
                            gallery.getChildren().add(createPhotoView(album.getPhotos().get(i)));
                        }
                    } else {
                        gallery.getChildren().add(new Label("No hay fotos aún . . ."));
                    }

                });
            }
        };
        thread.start();

    }

    private static void showOpenPhoto(Photo photo) {
        gallery.getChildren().clear();
        gallery.getChildren().add(createOpenPhotoView(photo));
    }

    public static void saveUsers() {
        try (ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream("users.bin"));) {
            salida.writeObject(users);
        } catch (Exception e) {
        }
    }

    public static void loadUsers() {
        try (ObjectInputStream entrada = new ObjectInputStream(new FileInputStream("users.bin"));) {
            users = (ArrayList<User>) entrada.readObject();
        } catch (Exception e) {
            users = new ArrayList<>();
            users.add(new User("Invitado", ""));
            Album album = new Album("Imágenes de muestra", "Imágenes de muestra Windows 7");
            album.getPhotos().add(new Photo("file:./muestra/B23.jpg", "", new Date(), album, "Koala"));
            album.getPhotos().add(new Photo("file:./muestra/107.jpg", "", new Date(), album, "Medusa"));
            album.getPhotos().add(new Photo("file:./muestra/ED0.jpg", "", new Date(), album, "Flor"));
            album.getPhotos().add(new Photo("file:./muestra/F0C.jpg", "", new Date(), album, "Cañón"));

            users.get(0).getAlbums().add(album);
            users.get(0).getAlbums().add(new Album("Graduación", "Graduacion Universidad 2015"));
            saveUsers();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
